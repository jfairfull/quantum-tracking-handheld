var user = {
	account: function(){
		$('#account-form').find('[name="company"]').val(user.session.user.company);
		$('#account-form').find('[name="name"]').val(user.session.user.name);
		$('#account-form').find('[name="email"]').val(user.session.user.email);
	},
	
	editAccount: function(t){
		api.call($(t).serialize()+'&token='+user.session.user.token, function(response){
			var db = new simpleDB('user');
			var session = db.get('session');
			session.user.company = response.output.user.company;
			session.user.name = response.output.user.name;
			session.user.email = response.output.user.email;
			user.session = session;
			db.put('session', session);
			$('#account-form').find('[name="company"]').val(response.output.user.company);
			$('#account-form').find('[name="name"]').val(response.output.user.name);
			$('#account-form').find('[name="email"]').val(response.output.user.email);
			alert('Account edited.');
		});
	},
	
	changeDevice: function(t){
		
		// TODO: add this logic in a more maintainable way for disconnecting all devices
		if (cordovaApp && typeof user.device != 'undefined'){
			console.log("checking if device is bluetooth");
			if (user.device.isBluetooth){
				
				if (typeof user.device.disconnectBluetoothDevice != 'undefined'){
					bluetoothSerial.disconnect();
					clearInterval(user.device.batteryCheckInterval);
					user.device.batteryPercentage = '';
					$('body').trigger('battery-indicator-update', user.device.batteryPercentage);
				}
			}
			else if (typeof user.device.disconnectDevice != 'undefined') {
				// add logic for removing buttons for native barcode and nfc scan
				console.log("calling device disconnect");
				user.device.disconnectDevice();
			}
		}
		console.log($(t).val());
		var prefs = (typeof t != 'undefined' ? new Preferences({
			'device':$(t).val()
		}) : new Preferences());
		user.prefs = prefs;
		switch(user.prefs.device) {
			case 'atid911' :
				user.device = atid911;
				break;
			case 'alrh450' :
				user.device = alrh450;
				break;
			case 'xc1003':
				user.device = xc1003;
				break;
			case 'nativeScan' :
				console.log("native scan select");
				user.device = nativeScan;
				break;
			default :
				user.device = tsl1128;
				break
		}
		
		if(user.device.isBluetooth === false) {
			$('#connectDevice').attr('disabled', true);

			user.device.init();
		} else {
			$('#connectDevice').removeAttr('disabled');
			
			// check the bluetooth connection, if a device is connected, call a function 
		}
		
		$('#connectDevice').val("Connect Device");
		
		// if (typeof user.device.checkConnectionStatus != 'undefined'){
		// 	user.device.checkConnectionStatus();
		// }
		
		Click('.finder', user.device.rangeFinder);
		
		// when user.device changes, we need to refresh the click listener
		$(document).off('click', '#connectDevice');
		$(document).on('click', '#connectDevice', user.device.init);
		
		Click('#closeRangeFinder', user.device.closeRangeFinder);
		Change('.power-level', user.device.changePowerLevel); // TODO: move changePowerLevel to a different namespace? Should cause error on tsl device.
		Change('#atid-scanType', user.device.switchScanType); 
		
		
	},
	
	forgotPassword: function(){
		var email = prompt('What is your email?', '');
		if(email) {
			api.call('action=forgotPassword&email='+email, function(response){
				alert('Please click the link in the email we sent you.');
			});
		}
	},
	
	loggedIn: function(){
		var db = new simpleDB('user');
		var session = db.get('session');
		if(session) {
			return true;
		} else {
			return false;
		}
		return false;
	},
	
	logOut: function(){
		var db = new simpleDB('user');
		db.del('session');
		window.location.href = window.location.href.split('#')[0];
	},
	
	saveApiKey: function(t){
		var key = $(t).val();
		user.prefs.apiKey = key;
		var prefs = new Preferences(user.prefs);
		user.prefs = prefs;
	}
};