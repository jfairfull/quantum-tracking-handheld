var project = {
	locationField:'',
	createReport: function(t){
		var list = ($(t).hasClass('createReport') ? 'Report' : 'List');
		if($('#tagDisplay pre').length) {
			var projects = [];
			$('#tagDisplay').find('pre').each(function(key, value){
				var project = $(this).attr('data-project');
				if($.inArray(project, projects) == -1) {
					projects.push(project);
				}
			});
			var go = true;
			if(projects.length > 1) {
				go = confirm('You have selected items from different projects. '+projects.length+' '+list.toLowerCase()+'s will be created.');
			}
			if(go) {
				var report = {};
				report.data = {};
				$.each(projects, function(key, projectId){
					$('#tagDisplay').find('pre').each(function(key, value){
						var project = $(this).attr('data-project');
						if(project == projectId) {
							var id = $(this).attr('data-id');
							report.data[id] = (list == 'Report' ? deparam($(this).attr('data-uri')) : id);
						}
					});
					var name = prompt(list+' Name (Required)', '');
					if(list == 'Report') {
						var notes = prompt('Notes (Not Required)', '');
					}
					if(name) {
						var method = (list == 'Report' ? 'proCreateReport' : 'proSaveList');
						var url = 'action='+method+'&project='+projectId+
						'&token='+user.session.user.token+
						'&'+$.param(report)+
						'&name='+name+
						'&notes='+notes;
						console.log(url);
						api.call(url, function(response){
							if(list == 'Report') {
								//alert(response.output.report);
								//window.open(response.output.report, '_blank', 'location=yes');
								alert("Report Created");
							} else {
								alert('List saved.');
							}
						});
					} else {
						alert('Please provide a name for the report.');
					}
				});						
			}
		} else {
			alert('You have not selected anything.');
		}
	},
	
	display: function(response, callback){
		$('#project').html('<table class="display responsive nowrap" cellspacing="0" width="100%"><thead></thead><tbody></tbody></table>');
		project.data = response.output;
		var d = {};
		$.each(project.data.data, function(key, value){
			//console.log(value);
			value.find = '<a data-id="'+key+'" class="finder" href="#">&#10148;</a>';
			value.tag = '<a data-id="'+key+'" class="writeTag" href="#tag">&#10148;</a>';
			value.tree = '<a data-id="'+key+'" class="tree-btn view-screen">&#10148;</a>';
			
			if (typeof value.image != 'undefined'){
				value.attach_image = '<a data-id="'+key+'" class="attachImage" href="#image">&#10148;</a>';
			}
			d[key] = value;
		});
		project.data.data = d;
		project.data.fields.push('find');
		project.data.fields.push('tag');
		project.data.fields.push('tree');
		console.log(project.data.fields);
		if (project.data.fields.join().match(/image/)){
			project.data.fields.splice(1, 0, 'attach_image');
		}
		var fields = [];
		var columns = [];
		$.each(project.data.fields, function(key, value){
			fields.push(library.key(value));
			columns.push({data:value});
		});
		$('#project thead').html('<tr><th>'+fields.join('</th><th>'));
		var table = $('#project table').dataTable({
			"deferRender": true,
			"createdRow": function(row, data, dataIndex) {
				if(typeof data.image != 'undefined') {
					if(data.image) {
						$(row).find('td').each(function(){
							var text = $(this).text();
							if(text.indexOf('http') != -1) {
								//console.log("rendering image: "+data.image);
								$(this).html('<img class="lazy" style="max-width:100px;height:auto;" src="'+data.image+'">');
							}
						});
					}
				}
			},
		    destroy: true,
		    responsive:true,
		    "data":library.toArray(project.data.data),
			"columns":columns
		});
		new $.fn.dataTable.Responsive(table);
		//$('img.lazy').lazyload();
		$.mobile.loading('hide');
		if(typeof callback != 'undefined') {
			callback();
		}
	},
	
	globalUpdate: function(t){
		var info = $(t).serialize();
		console.log("project's location field: "+ user.prefs.locationField);
		if (user.prefs.locationField != ''){
			console.log("info string: "+info);
			info = info.replace('location', user.prefs.locationField);
		}
			
		var number = $('#tagDisplay').find('pre').length;
		
		console.log("number of tags to update: "+number);
		var c = 0;
		$.mobile.loading('show');
		if(number) {
			$('#tagDisplay').find('pre').each(function(){
				var that = this;
				
				console.log("data-tag: "+$(this).attr('data-tag'));
				api.call('action=proTag&tag='+$(this).attr('data-tag')+'&token='+user.session.user.token+'&'+info, function(response){
					$(that).remove();
					item.display(response, response.output.info, 'rfid');
					c++;
					if(c >= number) {
						$.mobile.loading('hide');
						$.mobile.changePage('#find');
					}
				});
			});
		} else {
			$.mobile.loading('hide');
			alert('No tags.');
		}
	},
	
	load: function(t){
		var id = $(t).val();
		if(id) {
			console.log(typeof project);
			$.mobile.loading('show');
			api.call('action=proGet&project='+id+'&token='+user.session.user.token, function(response){
				project.display(response);
				interFace.getLists();
			}, function(){
				$('#project').html('<table class="display responsive nowrap" cellspacing="0" width="100%"><thead></thead><tbody></tbody></table>');
				$.mobile.loading('hide');
			});
		}
	},
	
	searchForm: function(){
		var p = $('#projects').val();
		if(p) {
			$.mobile.loading('show');
			api.call('action=getProjectMeta&project='+p+'&token='+user.session.user.token, function(response){
				var f = new Form({
					'form':response.output.form.form,
					'target':'#formModal .modal-body',
					'event':function(response){
						var data = {
							data:$('#formModal form').serializeObject()
						};
						api.call('action=proSearch&'+$.param(data)+'&project='+p+'&token='+user.session.user.token, function(response){
							project.display(response, function(){
								$.mobile.changePage('#view');
							});
						});
					}
				}, function(){
					forms.refresh('#formModal form');
					$('#formModal form').attr('data-ajax', 'false');
					$('#formModal form').find('button').parents('div:first').removeAttr('class');					
					$.mobile.changePage('#formModal');
					$('#formModal form').find('*').each(function(){
						$(this).removeAttr('required');
					});
				});					
			});
		}
	},
	
	writeTag: function(t){
		$.mobile.loading('show');
		$.mobile.changePage('#tag');
		var id = $(t).attr('data-id');
		api.call('action=proItem&project='+$('#projects').val()+'&id='+id+'&token='+user.session.user.token, function(response){
			user.device.activeTag = response.output.tag;
			$('#activeTag b').html(response.output.tag);
		});
		
		
		
		// call device write function, verify active tag using read single
	},
	attachImage: function(t){
		// load up either gallery or straight to camera. questions, should we give the user an option for either, load up the camera, or just load up the gallery
		
		console.log($(t).html());
		var $imagetag =  $(t).parent().parent().find('td > img');
	  
		if (cordovaApp){
			$.mobile.loading('show');
			navigator.notification.confirm(
			    'Choose an application to load in the image', 
			     function(i){
			     	var application;
			     	if (i == 1){
			     		application = Camera.PictureSourceType.CAMERA;
			     	}
			     	else if (i == 2){
			     		application = Camera.PictureSourceType.PHOTOLIBRARY;
			     	}
			     	else{
			     		$.mobile.loading('hide');
			     		return;
			     	}
			     	
			     	navigator.camera.getPicture(
			     		function(base64String){
			     			console.log(base64String.length);
		     				var projectID = $('#projects').val();
		     				var id = $(t).attr('data-id');
		     				
		     				// send attachment to server with the proper project and entry id
		     				api.call('action=proAttach&project='+projectID+'&id='+id+'&image='+encodeURIComponent(base64String)+'&token='+user.session.user.token, function(response){
								$.mobile.loading('hide');
								// response contains image url, update the url of the entry image tag if it exists
								var $imagetag = $(t).parent().find('td > image');
								if ($imagetag.length > 0){
									console.log("updating image tag");
									$imagetag.html('<img style="max-width:100px;height:auto;" src="'+response+'">');
								}
								else{
									// refresh datatable
									console.log("refreshing projects view");
									project.load('#projects');
								}
							}, function(error){
								console.log(error);
								alert("Unable to update image data for given row");
								$.mobile.loading('hide');
							});
							
							// we need to update the image after the post 
							console.log("looking for image tag on the current row")
							console.log($(t).html());
							console.log($(t).parent().html());
							if ($imagetag.length > 0){
								console.log("updating image tag");
								$imagetag.attr('src', 'data:image/jpeg;base64,'+base64String);
								console.log('data:image/jpeg;base64,'+base64String+'; finished base64');
							}
			     			
			     		},
				     	function(error){
				     		alert(error);
				     		$.mobile.loading('hide');
				     	},
				     	{'sourceType':application, 'destinationType':Camera.DestinationType.DATA_URL}
				     );
				     
			     },            
			    'Attach Image',          
			    ['Camera','Gallery']    
			);
		}
	},
	loadList: function(t){
		var id = $(t).val();
		
		// get current project
		var projectType = $('#projects').val();
		
		if (id && project){
			$.mobile.loading('show');
			api.call('action=proListFromProject&project='+projectType+'&id='+id+'&token='+user.session.user.token, function(response){
				project.display(response);
			},
			function(){
				$('#project').html('<table class="display responsive nowrap" cellspacing="0" width="100%"><thead></thead><tbody></tbody></table>');
				$.mobile.loading('hide');
			});
		}
	}
};