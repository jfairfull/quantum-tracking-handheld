var inventory = {
    list:{},
    scannedList:{},
    notInLocationList:{},
    inLocationList:{},
    inWrongLocationList:{},
    idList:{},
    listIndex:'',
    locationField:'location',
    hasRunInventory: false,
    networkCall: {},
    createReport: function(t){
        console.log("entering create report function");
		var list = ($(t).hasClass('inventoryReport') ? 'Report' : 'List');
		var notInListReport = ($(t).hasClass('inventoryNotInListReport'));
		var displaySelector = notInListReport ? '#nil' : '#inventoryDisplay';
		//console.log(list);
		//console.log(displaySelector);
		//console.log(displaySelector+' pre');
		//console.log($(displaySelector+' pre').length);
		if($(displaySelector+' pre').length) {
			var projects = ['inventory'];
			$(displaySelector).find('pre').each(function(key, value){
				var project = $(this).attr('data-project');
				console.log(project);
				project = 'inventory';
				if($.inArray(project, projects) == -1) {
					projects.push(project);
				}
			});
			var go = true;
			if(projects.length > 1) {
				go = confirm('You have selected items from different projects. '+projects.length+' '+list.toLowerCase()+'s will be created.');
			}
			if(go) {
				var report = {};
				report.data = {};
				$.each(projects, function(key, projectId){
					$(displaySelector).find('pre').each(function(key, value){
						var project = $(this).attr('data-project');
						project = 'inventory';
						if(project == projectId) {
						    console.log($(this).outerHTML());
							var id = $(this).attr('data-id');
							console.log("id to be added to list: "+id);
							console.log("adding report data");
							report.data[id] = (list == 'Report' ? deparam($(this).attr('data-uri')) : id);
						}
					});
					var name = prompt(list+' Name (Required)', '');
					if(list == 'Report') {
						var notes = prompt('Notes (Not Required)', '');
					}
					if(name) {
					    if (typeof projectId == 'undefined'){
					        projectId = 'inventory';
					    }
					    console.log(report);
					    console.log(report.data);
						var method = (list == 'Report' ? 'proCreateReport' : 'proSaveList');
						var url = 'action='+method+'&project='+projectId+
						'&token='+user.session.user.token+
						'&'+$.param(report)+
						'&name='+name+
						'&notes='+notes;
						console.log(url);
						api.call(url, function(response){
							if(list == 'Report') {
								//alert(response.output.report);
								console.log(response.output.report);
								//window.open(response.output.report, '_blank', 'location=yes');
								alert("Report created");
							} else {
								alert('List saved.');
							}
						}, function(response){
						    console.log("Error posting report");
						    console.log(response);
						    alert("Error in creating report");
						});
					} else {
						alert('Please provide a name for the report.');
					}
				});						
			}
		} else {
			alert('You have not selected anything.');
		}
	},
    changeLocation: function(t){
        var l = $(t).val();
        $('#inventoryLocation b').html(l);
        inventory.location = l;
        $.mobile.changePage('#inventory');
        $('#inventory').trigger('locationConfirmed');
    },
    
    clear: function(){
        app.clearFinder();
        inventory.list = {};
        inventory.notInLocationList = {};
        inventory.inLocationList = {};
        inventory.idList = {};
        inventory.listIndex = '';
        inventory.hasRunInventory = false;
        $('#inventoryDisplay').html('');
        $('#inventoryLocation b').html('');
        $('#inventoryLocation b').html('');
        $('#inventoryItemsFound b').html('0');
        $('#il').html('');
        $('#nil').html('<button id="inventoryNotInListReport" class="ui-btn inventoryNotInListReport" disabled="disabled">Create List</button>');
        $('#iwl').html('');
        //$('#inventoryLists').removeAttr('disabled');
        inventory.ui();
        $.mobile.loading('hide');
    },
    
    display: function(value, sel){
        
        var data_id = value.id;
        delete value.id;
        var selector = (typeof sel == 'undefined' ? '#inventoryDisplay' : sel);
        var html = library.displayObject(value);
        $(selector).append(html);

        if (typeof data_id != 'undefined'){
            var $treebtn = $('<a data-id="'+data_id+'" class="tree-btn">View Tree</a>');
            $(selector).find('pre:last-child').attr('data-id', data_id);
            $(selector).find('pre:last-child')
            .attr('data-uri', $.param(value)).after($treebtn);
            
            //console.log($(selector).find('pre:last-child').outerHTML());
        }
        else{
            $(selector).find('pre:last-child')
            .attr('data-uri', $.param(value));
        }
        
    },
    
    getLists: function(){
        api.call('action=proGetLists&token='+user.session.user.token, function(response){
            $('#inventoryLists').html('<option value="">Please Choose</option>');
            $('.inventoryLists').html('<option value="">Please Choose</option>');
            if(response.output) {
                $.each(response.output, function(key, value){
                    //console.log(key);
                    $('#inventoryLists').append('<option value="'+key+'">'+value.name+'</option>');
                    $('.inventoryLists').append('<option value="'+key+'">'+value.name+'</option>');
                });
                $('#inventoryLists').selectmenu().selectmenu('refresh');
                $('.inventoryLists').selectmenu().selectmenu('refresh');
            }
        });
    },
    selectList: function(t){
        // once a list is selected. 
        
        // when switching from a list to a new list, we must remove previous entries that were added to in location and not in location
        var index = $(t).val();
        if (inventory.listIndex == index){
            // this list was already selected
            alert('List already selected');
            return;
        } else if (inventory.listIndex != ''){
            // remove tags that are in the not in location screen and any tags that were added to in location that were not scanned in.
        //     $('#nil').find('pre').each(function(){
		      // var inventoryObject = deparam($(this).attr('data-uri'));
		      // if (inventoryObject.tag == inventory.notInLocationList[inventoryObject.tag]){
		      //     $(this).remove();
		      //     delete inventory.notInLocationList[inventoryObject.tag];
		      // }
		      // else if (inventoryObject.barcode == inventory.notInLocationList[inventoryObject.barcode]){
		      //     $(this).remove();
		      //     delete inventory.notInLocationList[inventoryObject.barcode];
		      // }
        //     });
            
        //     $('#il').find('pre').each(function(){
		      // var inventoryObject = deparam($(this).attr('data-uri'));
		      // if (inventoryObject.tag == inventory.inLocationList[inventoryObject.tag] && inventoryObject.tag != inventory.list[inventoryObject.tag]){
		      //     $(this).remove();
		      //     delete inventory.inLocationList[inventoryObject.tag];
		      // }
		      // else if (inventoryObject.barcode == inventory.inLocationList[inventoryObject.barcode] && inventoryObject.barcode != inventory.list[inventoryObject.barcode]){
		      //     $(this).remove();
		      //     delete inventory.inLocationList[inventoryObject.barcode];
		      // }
        //     });
        
            inventory.clear();
        }
        
        inventory.listIndex = index;
        console.log(index);
        
        var request = [];
		
		api.call('action=proList&'+"id="+index+'&token='+user.session.user.token, function(response){
            $.each(response.output, function(key, value){
                //console.log(user.device.decodeProcessedTag(value.tag));
                // value.id = key;
                inventory.notInLocationList[value.tag] = value.tag;
                inventory.notInLocationList[value.barcode] = value.barcode;
                console.log("barcode from proList: " + value.barcode);
                inventory.display(value, '#nil');
                value.id = key;
               
                inventory.display(value, '#inventoryDisplay');
                inventory.removeDuplicates(['#inventoryDisplay']);
                
            
                inventory.refresh(value.tag);
                inventory.refresh(value.barcode);
                
            });
            
            $('#inventoryFunctions button').removeAttr('disabled');
		
    		if ($('#nil').find('pre').length > 0){
                $('.inventoryNotInListReport').removeAttr('disabled');
            }
            else{
                $('.inventoryNotInListReport').prop('disabled', 'disabled');
            }
            
		});
            
		$('#inventoryPanel').panel('close');
    },
    appendList: function(t){
     var data = deparam($(t).serialize());
     var listID = data.inventoryListId;
     
     if($('#inventoryDisplay').length) {
            var n = $('#inventoryDisplay').length;
            var c = 0;
			$('#inventoryDisplay').find('pre').each(function(key, value){
			    var tagID = $(this).attr('data-id');
			    
			    api.call('action=proAppendList&id='+listID+'&data='+tagID+'&token='+user.session.user.token, function(response){
			       c++;
			       console.log(response);
			       if (c >= n){
			        alert("List appended");
			       }
			    }, function(response){
			        console.log("Error in appending list");
			        console.log(response);
			        alert("Failed to append list");
			    });
			});
     }else{
         alert('You have not selected anything.');
     }
    },
    getLocation: function(callback){
        var query = 'key='+inventory.locationField+'&val='+inventory.location;
        api.call('action=proQuery&'+query+'&token='+user.session.user.token, function(response){
            $.each(response.output.data, function(key, value){
                value.id = key;
                //inventory.display(value, '#nil');
                //inventory.notInLocationList[value] = value;
                console.log("key: " + key);
                if (key != inventory.idList[key]){
                    console.log("found new key, displaying value...");
                    inventory.display(value);
                }
                console.log("value barcode in get location: " + value.barcode);
                console.log("value tag in get location: " + value.tag);
                
            });
            inventory.hasRunInventory = true;
            if(typeof callback == 'function') {
                callback();
            }            
        });        
    },
    
    init: function(){
    	$.extend(inventory.list, user.device.scanned);
    	inventory.ui();
    	//$(document).on('rfid_stop', inventory.processNetworkCalls);
    	$.mobile.loading('hide');
    },
    refresh: function(tag, type){
        
        if (typeof type == 'undefined'){
            type = 'tag'
        }
        
        // not in location are added to inventory.list and a new special list
        // as new items are scanned in, check to see if any of the new scans match with the special list
        // if a match exists, remove the tag from the special list, and move the tag in the not in location view to in location.
        $.extend(inventory.list, user.device.scanned);
        var inventoryLength = Object.keys(inventory.list).length;
 
        tag = tag.toLowerCase();
        
    	
    	$('#inventoryItemsFound b').html(inventoryLength);
    	if(inventoryLength) {
    	    $('#inventoryFunctions button').removeAttr('disabled');
    	} else {
    	    $('#inventoryFunctions button').prop('disabled', 'disabled');
    	}
    	
    	console.log("Looking for tag in not in location list: " + inventory.list[tag] + " not in location? : "+ inventory.notInLocationList[tag]);
    	if (inventory.list[tag] == inventory.notInLocationList[tag]){
    	    // we have a match, remove it from the not in location view to in location/in wrong location.
    	    
    	    $('#nil').find('pre').each(function(){
		       var inventoryObject = deparam($(this).attr('data-uri'));
		       console.log("barcode from uri: " +  inventoryObject.barcode);
		       if (inventoryObject.tag == inventory.notInLocationList[tag] || inventoryObject.barcode == inventory.notInLocationList[tag]){
		           inventory.display(inventoryObject, "#il");
		           //inventory.display(inventoryObject);
		           $(this).remove();
		       }
    	       //console.log(data.attr('data-uri')); 
    	    });
    	    delete inventory.notInLocationList[tag];
    	    inventory.inLocationList[tag] = tag;
    	    
    	    if ($('#nil').find('pre').length > 0){
                $('.inventoryNotInListReport').removeAttr('disabled');
            }
            else{
                $('.inventoryNotInListReport').prop('disabled', 'disabled');
            }
            $.mobile.loading('hide');
        }
        	
    
        // make api call using pro find tag to get the tag id and compare the id of the gotten tag to the id of the tags in not in location
        else if (inventory.hasRunInventory){
            //$.mobile.loading('show');
            var t = tag;
            if(t.length == 6 || t.length == 12 || t.length == 13) {
			    var found = false;
			 //   inventory.networkCall[t].type = type;
			 //   inventory.networkCall[t].tag = t;
			 //   inventory.networkCall[t].token = user.session.user.token;
			 //   inventory.networkCall[t].method = 'proFindTag';
    			api.call('action=proFindTag&'+type+'='+t+'&token='+user.session.user.token, function(response){
    				var removedFromNil = false;
    				$('#nil').find('pre').each(function(){
    				    var inventoryObject = deparam($(this).attr('data-uri'));
    				    if ($(this).attr('data-id') == response.output.id){
    				        inventory.display(inventoryObject, "#il");
        		            $(this).remove();
        		            removedFromNil = true;
    				    }
    				});
    				
    				if (removedFromNil){
    				   if (typeof inventory.notInLocationList[tag] != 'undefined'){
                        delete inventory.notInLocationList[tag];
    				   }
    	               
    				}
    				
    				$.mobile.loading('hide');
    				
    			}, function(){
    				$.mobile.loading('hide');
    				
    			});
            }
        }
        else{
            $.mobile.loading('hide');
        }
        
        
        user.device.processingTag = false;
    },
    processNetworkCalls: function(){
        for (var call in inventory.networkCall){
            if (inventory.networkCall.hasOwnProperty(call)){
                
                api.call('action='+inventory.networkCall[call].method+'&'+inventory.networkCall[call].type+'='+inventory.networkCall[call].tag+'&token='+inventory.networkCall[call].token, function(response){
    				var removedFromNil = false;
    				$('#nil').find('pre').each(function(){
    				    var inventoryObject = deparam($(this).attr('data-uri'));
    				    if ($(this).attr('data-id') == response.output.id){
    				        inventory.display(inventoryObject, "#il");
        		            $(this).remove();
        		            removedFromNil = true;
    				    }
    				});
    				
    				if (removedFromNil){
    				   if (typeof inventory.notInLocationList[tag] != 'undefined'){
                        delete inventory.notInLocationList[tag];
    				   }
    	               
    				}
    				
    				$.mobile.loading('hide');
    				
    			}, function(){
    				$.mobile.loading('hide');
    				
    			});
            }
        }
    },
    run: function(){
    	var d = {tags:inventory.list};
    	api.call('action=proInventory&'+$.param(d)+'&token='+user.session.user.token, function(response){
    	    console.log("calling success callback");
    	    console.log(response);
    	    console.log(response.output);
    	    if(response.output.locationField) {
    	        inventory.locationField = response.output.locationField;
    	    }
    		if(response.output.location) {
    		    console.log("Location field: " + inventory.locationField);
    		    //$('#inventoryDisplay').html('');
    		    //$('#il').html('');
                //$('#nil').html('');
                $('#iwl').html('');
    		    $.each(response.output.items, function(key, value){
    		        value.id = key;
    		        inventory.display(value);
    		        inventory.idList[key] = key;
    		        console.log('Barcode being displayed in main all view: '+value.barcode);
    		        console.log('Tag being displayed in main all view: '+value.tag);
    		        
    		    });
    			if(confirm('Your current location is: '+response.output.location)) {
    			    inventory.location = response.output.location;
    				$('#inventoryLocation b').html(response.output.location);
    				$('#inventory').trigger('locationConfirmed');
    			} else {
    				if(response.output.locations) {
    					$("#inventoryLocations select").html('<option value="">Please Choose</option>');
    					$.each(response.output.locations, function(key, value){
    						$("#inventoryLocations select").append('<option value="'+value+'">'+value+'</option>');
    					});
	    				$.mobile.changePage("#inventoryLocations");
    				}
    			}
    		}
    		
    		$.mobile.loading('hide');
    	},
    	function(err){
    	    console.log("calling error callback");
    	    console.log(err);
    	});        
    },
    globalUpdate: function(t){
		var info ={}; // $(t).serialize();
		
		var number = $('#iwl').find('pre').length;
		var c = 0;
		$.mobile.loading('show');
		if(number) {
			$('#iwl').find('pre').each(function(){
				var that = this;
				var data = deparam($(this).attr('data-uri'));
				var date = new Date();
				var date_string = (date.getMonth() + 1) + '/'+date.getDate() +'/'+ date.getFullYear();
				
				info = {
				  'status': typeof data['status'] != 'undefined' ?  data['status'] : 'inventory '+date_string,
				};
				info[inventory.locationField] = inventory.location;
				console.log("info string: " + $.param(info));
				console.log("update query: " + 'action=proTag&tag='+data['tag']+'&token='+user.session.user.token+'&'+$.param(info));
				api.call('action=proTag&tag='+data['tag']+'&token='+user.session.user.token+'&'+$.param(info), function(response){
					$(that).remove();
					//item.display(response, t, 'rfid');
					inventory.display(response.output.data, '#il');
					c++;
					if(c >= number) {
						$.mobile.loading('hide');
						$('#inventoryPanel').panel('close');
						//$.mobile.changePage('#find');
					}
				}, function(response){
				    console.log(response);
				    $.mobile.loading('hide');
				    alert('Error updating tag in location: ' +  data[inventory.locationField]);
				});
			});
		} else {
			$.mobile.loading('hide');
			alert('No tags.');
		}
	},
    sort: function(){
        $.mobile.loading('show');
        var query = inventory.locationField+'='+inventory.location;
        console.log(query);
        // TODO: also check the tags that are in location and move them to iwl if need be
        $('#inventoryDisplay').find('pre').each(function(){
            // TODO: sort based on id
            var that = $(this);
            var d = deparam($(this).attr('data-uri'));
            var entryID = $(this).attr('data-id');
            console.log("Data id: " + entryID);
            if(typeof d[inventory.locationField] != 'undefined') {
                var item = $(this).outerHTML();
                console.log('Tag being sorted: '+d['barcode']);
                
                if(d[inventory.locationField] == inventory.location && 
                entryID == inventory.idList[entryID]) {
                    
                    $('#il').append(item);
                } else if (d[inventory.locationField] == inventory.location && 
                 entryID != inventory.idList[entryID]) {
                    $('#nil').append(item);
                    //inventory.notInLocationList[d.tag] = d.tag;
                    //inventory.notInLocationList[d.barcode] = d.barcode;
                } else if (d[inventory.locationField] != inventory.location) {
                    inventory.inWrongLocationList[d.tag] = d.tag;
                    $('#iwl').append(item);
                }
            }
        });
        
        $('#il').find('pre').each(function(){
            var that = $(this);
            var d = deparam($(this).attr('data-uri'));
            var entryID = $(this).attr('data-id');
            var item = $(this).outerHTML();
            console.log("Data id: " + entryID);
            if(typeof d[inventory.locationField] != 'undefined') {
                if (d[inventory.locationField] != inventory.location) {
                    inventory.inWrongLocationList[d.tag] = d.tag;
                    $('#iwl').append(item);
                    $(this).remove();
                }
            }
        });
        
        inventory.removeDuplicates();
        
        if ($('#nil').find('pre').length > 0){
            $('.inventoryNotInListReport').removeAttr('disabled');
        } else {
            $('.inventoryNotInListReport').prop('disabled', 'disabled');
        }
        
        $.mobile.loading('hide');
        
        $('#inventoryPanel').panel('close');
    },
    removeDuplicates : function(t){
        var views = [];
        if (typeof t == 'undefined'){
            views = ['#iwl', '#il', '#nil', '#inventoryDisplay'];
            
            // $('#iwl', '#nil', '#il').find('pre').each(function(){
            //     var uri = $(this).attr('data-uri');
            //     var count = $(views[i]+' pre[data-uri="'+uri+'"]').length;
            //     console.log(uri);
            //     console.log(count);
            //     if(count > 1) {
            //         $(this).remove();
            //     }
            // });
        }
        else if (typeof t == 'array') {

            views = t;
        }
        else{

            views.push(t);
        }
        
        for (var i = 0; i < views.length; i++){
            $(views[i]).find('pre').each(function(){
                var uri = $(this).attr('data-uri');
                var id = $(this).attr('data-id');
                var count = $(views[i]+' pre[data-uri="'+uri+'"]').length;
                var idCount = $(views[i]+' pre[data-id="'+id+'"]').length;
                console.log(uri);
                console.log(count);
                if(count > 1 || idCount > 1) {
                    $(this).remove();
                }
            });
        }
        
    },
    ui: function(){
        app.clearFinder();
        inventory.getLists();
    	var inventoryLength = Object.keys(inventory.list).length;
    	$('#inventoryItemsFound b').html(inventoryLength);
    	if(inventoryLength) {
    	    $('#inventoryFunctions button').removeAttr('disabled');
    	} else {
    	    $('#inventoryFunctions button').prop('disabled', 'disabled');
    	}
    },
    
    update: function(t){
        
    }
};