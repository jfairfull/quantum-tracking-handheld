var forms = {
	login:{
		"field1": {
			"name": "email",
			"type": "",
			"label": "Email",
			"placeholder": "",
			"required": "yes",
			"maxlength": ""
		},
		"field2": {
			"name": "password",
			"type": "password",
			"label": "Password",
			"placeholder": "",
			"required": "yes",
			"maxlength": ""
		},
		"field3": {
			"name": "action",
			"type": "hidden",
			"value":"login"
		},
	},
	
	refresh: function(form, data){
		$(form).find('*').each(function(){
			var tag  = String($(this).prop('tagName')).toLowerCase();
			var type = $(this).attr('type');
			switch(tag) {
				case 'button' :
					$(this).button('refresh').addClass('ui-btn');
					break;
				case 'select' :
					var name = $(this).attr('name');
                    $(this).find('option').each(function(){
                    	if(typeof data != 'undefined') {
	                       	if($(this).attr('value') == data[name]) {
    	                        $(this).attr('selected', true);
        	               	} else {
            	           		$(this).removeAttr('selected');
                	       	}
                    	}
                    });
					$(this).selectmenu().selectmenu("refresh", true);
					break;
				case 'textarea' :
					$(this).textinput().textinput("refresh");
					break;
				default :
					if(type == 'checkbox' || type == 'radio') {
						$(this).checkboxradio().checkboxradio("refresh")
					} else {
						$(this).textinput().textinput("refresh");
					}
					break;
			}
		});
	}
};