var alrh450 = {
    isBluetooth:false,
    className: 'alrh450',
    scanType: '',
    rfidMode: enums.rfidMode.READ,
    range:{
        low: -85,
        high: -30
    },
    hasInitialized: false,
    isWriting : false,
    powerLevel: 300,
    myactiveTag: '3000AD9009001B6BFD9662000018',
    tagsInRange: {},
    
    // TODO: create enums/objects for rfid modes and rfid write fail types. might be useful to create a separate atid state manager for modes/boolean flags
    // TODO: implement continuous burst for inventory screen while find view does single read?
    init: function(isReset){
        console.log('alrh450!!!');
        
        // populate widgets with power level and scan type from preferences, TODO: initialize these elsewhere in a different namespace
        if (typeof isReset == 'undefined'){
            user.device.populatePowerLevel();
            user.device.loadScanType();
        }
        
        if(cordovaApp) {
            user.device.tagsInRange = {};
            user.device.switchScanType('#atid-scanType');
            user.device.enableTrigger();
        }
      
    },
    loadScanType: function(){
        if (typeof user.prefs == 'undefined')
        	user.prefs = new Preferences();
        
        if (typeof user.prefs.scanType == 'undefined')
            user.prefs = new Preferences({'scanType':'RFID'});
            
        $('#atid-scanType').val(user.prefs.scanType);
	
      
    },
    switchScanType : function(t){
        var type = $(t).val();
        
        switch(type){
            case(enums.scanType.RFID):
                user.device.rfidInit();
                user.device.scanType = type;
                break;
            case(enums.scanType.BARCODE):
                user.device.barcodeInit();
                user.device.scanType = type;
                break;
            
        }
        if (typeof user.prefs == 'undefined')
            user.prefs = new Preferences();
            
        user.prefs.scanType = user.device.scanType;
        
        user.prefs = new Preferences({'scanType':user.prefs.scanType});
    },
    rfidInit: function() {
        var initilizeRfidDevice = function(msg){
            if (typeof msg !== 'undefined'){
                console.log(msg);
            }
            
            alien.rfid.open_reader(function(msg){
                console.log(msg);
                user.device.hasInitialized = true;
                user.device.changePowerLevel();
            }, user.device.printMsg);
            alien.rfid.onReaderReadTag(user.device.readTagCallback, user.device.printMsg);  
        };
        
        if (user.device.scanType == enums.scanType.BARCODE){
            alien.barcode.stop_scan(initilizeRfidDevice, initilizeRfidDevice);
        }
        else{
           initilizeRfidDevice("initializing rfid device"); 
        }
      
    },
    barcodeInit : function() {
        var initializeBarcodeDevice = function(msg){
            if (typeof msg !== 'undefined'){
                console.log(msg);
            }
            
            alien.barcode.onScan(function(scanResults){
                if($.mobile.activePage.attr("id") != 'rangeFinder') {
                    user.device.go(scanResults);
                }
            });
        }
        user.device.hasInitialized = false;
        if (user.device.scanType == enums.scanType.RFID){
            alien.rfid.stop_scan(alien.rfid.close_reader(initializeBarcodeDevice, user.device.printMsg), user.device.printMsg);
        }
        else{
           initializeBarcodeDevice("initializing barcode device");
        }
            
    },
    disableTrigger : function(){
	    return (alien.general.onTriggerUp() == alien.general.onTriggerDown());
	},
	enableTrigger : function() {
	    alien.general.onTriggerUp(user.device.onTriggerUp);
       // alien.general.onTriggerdown(function(){console.log("hello world")});
	},
	onTriggerDown : function(){
	  switch (user.device.scanType){
            case (enums.scanType.RFID):
                if (user.device.rfidMode == enums.rfidMode.READ){
                    if (!user.device.isReading && !user.device.processingTag){
                          alien.rfid.isRunning(function(msg){
                              if (msg == 'false'){
                                  user.device.isReading = true; 
                                  alien.rfid.start_readTagContinuous(function(msg){console.log(msg);}, user.device.printMsg);
                              }
                          }, user.device.printMsg);
                    }
                }
                else if (user.device.rfidMode == enums.rfidMode.WRITE){
                    if (typeof user.device.activeTag == 'undefined' || user.device.activeTag == ''){
                        console.log("active tag not found, aborting write...");
                        user.device.writeRfidError("active tag not found, aborting write...");
                        return;
                    }
                        
                    var data = '';
                    for (var i = 0; i < user.device.activeTag.length; i++){
                        data += dec2hex(user.device.activeTag.charCodeAt(i));
                    }
                    
                    console.log(data);
                    
                    if (user.device.disableTrigger())
                    {
                        user.device.writeRfid(data);
                    }
                }
                else if (user.device.rfidMode == enums.rfidMode.INVENTORY){
                    alien.rfid.isRunning(function(msg){
                          if (msg == 'false'){
                              user.device.isReading = true; 
                              atid.rfid.start_readTagContinuous(function(msg){console.log(msg);}, user.device.printMsg);
                          }
                      }, user.device.printMsg);
                }
                
                break;
            
            case(enums.scanType.BARCODE):
                
                alien.barcode.isRunning(function(msg){
                    if (msg == 'false')
                    {
                        alien.barcode.start_scan();
                    }
                }, user.device.printMsg);
                
                break;
        }  
	},
	onTriggerUp : function(){
	   switch (user.device.scanType){
            case ('RFID'):
                alien.rfid.stop_scan(user.device.printMsg, user.device.printMsg);
                break;
            
            case('Barcode'):
                alien.barcode.stop_scan(user.device.printMsg, user.device.printMsg);
                break;
        } 
	},
    readTagCallback : function(data) {
          if (user.device.rfidMode == enums.rfidMode.READ || user.device.rfidMode == enums.rfidMode.INVENTORY){
            user.device.processingTag = true;
            console.log("Tag : " + data.tag + " RSSI: " + data.rssi);
            if($.mobile.activePage.attr("id") != 'rangeFinder') {
                user.device.go(data.tag, 'RFID');
            }
          }
          
          // If writing, proceed with verification step
          else if (user.device.rfidMode == enums.rfidMode.WRITE){
            console.log("Tag : " + data.tag + " RSSI: " + data.rssi);
            user.device.tagsInRange[data.tag] = data.tag;
            var tag = user.device.processRawTag(data.tag);
            
            if (tag == user.device.activeTag){
                 $.mobile.loading('hide'); 
                 console.log("RFID Write succeeded");
                 user.device.enableTrigger();
                 user.device.enableUI();
                 
                 alert("RFID Write Succeeded");
            }
            else{
                 user.device.writeRfidError("Verification fail");
            }
          }
          
          else if (user.device.rfidMode == enums.rfidMode.REVOKE){
              console.log("Tag : " + data.tag + " RSSI: " + data.rssi);
              var tag = data.tag.length == 28 ? $.trim(data.tag.substring(4, data.tag.length)) : $.trim(data.tag);
              if (tag == '000000000000000000000000'){
                 $.mobile.loading('hide'); 
                 console.log("RFID revoke succeeded");
                 user.device.enableTrigger();
                 user.device.enableUI();
                 
                 alert("RFID Revoke Succeeded");
            }
            else{
                 user.device.writeRfidError("Possible revoke fail");
            }
          }
          
          else if (user.device.rfidMode == enums.rfidMode.FIND){
            
            if (typeof user.device.tagsInRange[data.tag] == 'undefined'){
                user.device.tagsInRange[data.tag] = data.tag;
                var t = user.device.processRawTag(data.tag);
                if (user.device.tagToFind == t){
                     console.log("Tag : " + data.tag + " RSSI: " + data.rssi);
                    //atid.general.playSound('beep');
                    $('#range').val(data.rssi).slider("refresh");
                }
            }
              
          }
    },
    writeTagCallback : function(data){
        console.log('code: ' + data.code + ' action: ' + data.action + 'epc: ' + data.epc + 'data: ' + data.data);
        
        if (user.device.rfidMode == enums.rfidMode.WRITE || user.device.rfidMode == enums.rfidMode.REVOKE){
        
            if (data.code == "Out of retries"){
                user.device.writeRfidError(data.code);
                
            }
            else{
                user.device.writeOffset += 1;
                setTimeout(function(){
                    user.device.recursiveWrite(user.device.writeOffset, user.device.writeData);
                    
                }, 1000); 
            }
        }
    },
    //TODO: make the process raw tag and decode processed tag in a separate namespace
    processRawTag: function(data){
        return invengo_device.processRawTag(data);
    },
    decodeProcessedTag: function(tag){
        var data = '';
        for (var i = 0; i < tag.length; i++){
            data += dec2hex(tag.charCodeAt(i));
        }
        
        console.log(data);
        return data;
    },
    go: function(data, type){
		if(typeof data != 'undefined') {
		    console.log(user.device.scanType);
		    console.log("type: "+type);
			if (user.device.scanType == enums.scanType.RFID || type == enums.scanType.RFID){
			    if (typeof user.device.raw[data] == 'undefined') {
			        // if we are reading in a new tag into the system, shut down key event listener, stop any scan, process tag/find, then reinitialize
			        user.device.raw[data] = data;
            		$.mobile.loading('show');
            		var t = user.device.processRawTag(data);
            		
            	    console.log('Encoded RFID value: ' + t);
            	    t = t.toLowerCase();
            		
            		if(typeof user.device.scanned[t] == 'undefined') {
            		    t = t.toLowerCase();
            			user.device.scanned[t] = t;
            			//atid.general.playSound('beep');
            			
            			if($.mobile.activePage.attr("id") == enums.rfidMode.INVENTORY) {
            				inventory.refresh(t);
            			} else {
            				user.device.find(t, 'rfid');
            			}
            		}
            		else
            		    user.device.processingTag = false;
    			}
    			else
    			   user.device.processingTag = false; 
			}
            else if (user.device.scanType == enums.scanType.BARCODE || type == enums.scanType.BARCODE){
        		$.mobile.loading('show');
        		var x = data;
        		x = x.toLowerCase();
        		if(typeof user.device.scanned[x] == 'undefined') {
        		    x = x.toLowerCase();
        			user.device.scanned[x] = x;
        			//user.device.find(x, 'barcode');
        			if($.mobile.activePage.attr("id") == enums.rfidMode.INVENTORY) {
            			inventory.refresh(x, 'barcode');
        			} else {
        				user.device.find(x, 'barcode');
        			}
    				
    			}
            }
			
		}
	},
	find: function(t, type){
		var myType = (type == 'undefined' ? 'rfid' : type);
		var apiCallType = (myType == 'rfid' ? 'tag' : 'barcode');
		if(t.length == 6 || t.length == 12 || t.length == 13) {
			var found = false;
			api.call('action=proFindTag&'+apiCallType+'='+t+'&token='+user.session.user.token, function(response){
				item.display(response, t, 'rfid');
				console.log("Found tag in remote server!");
				$.mobile.loading('hide');
				user.device.processingTag = false;
			}, function(){
				$.mobile.loading('hide');
				user.device.processingTag = false;
			});
			$('body').trigger('findTag');
		} else {
			//alert('The tag is not valid.');
			console.log('The tag is not valid.');
			user.device.processingTag = false;
		}
	},
	rangeFinder: function(t){
	    console.log("Opening range finder");
	    var abortRangeFinder = function(msg){
	        if (typeof msg != 'undefined'){
	            console.log(msg);
	        }
	        $.mobile.loading('hide');
            $.mobile.changePage('#home');
            user.device.enableTrigger();
	    }
	    if (user.device.disableTrigger()){
        	$('#range').attr('min', user.device.range.low);
        	$('#range').attr('max', user.device.range.high);
        	$.mobile.changePage('#rangeFinder');
        	$.mobile.loading('show');
        	var readCount = 0;
        	var resetReadCount = 10;
        	var id = $(t).attr('data-id');
        	api.call('action=proItem&project='+$('#projects').val()+'&id='+id+'&token='+user.session.user.token, function(response){
        		$.mobile.loading('hide');
        		if(cordovaApp && response.output) {
        		    user.device.tagToFind = response.output.tag;
        		    alien.rfid.isRunning(function(msg){
                          if (msg == 'false'){
                            user.device.isScanning = true;
                            user.device.scanBurst();
                          }
                          else{
                              abortRangeFinder("Rfid device is still running");
                          }
                    }, abortRangeFinder);
    		   
        		}
        	});
	    }
	},
	scanBurst : function(){
	    if (!user.device.isScanning)
	        return;
	        
	    alien.rfid.stop_scan(function(){
	        user.device.tagsInRange = {};
	        setTimeout(function(){
	            if (user.device.isScanning){
    	            alien.rfid.start_readTagContinuous(function(){
        	            setTimeout(function(){
        	                if (user.device.isScanning == true)
        	                    user.device.scanBurst();
        	            }, 500);
	                }, user.device.printMsg, false);
	            }
	        }, 2000);
	    }, user.device.printMsg);
	},
	closeRangeFinder: function(){
	    console.log("closing range finder");
	    user.device.isScanning = false;
		alien.rfid.stop_scan(user.device.printMsg, user.device.printMsg);
		user.device.tagsInRange = {};
		
		user.device.tagToFind = '';
		$.mobile.changePage('#view');
		user.device.enableTrigger();
	},
	writeRfid : function(data, format) {
	    user.device.tagsInRange = {};
	    if (data.length != 24){
	        console.log("invalid tag to write, aborting..");
	        return;
	    }
	    
	    user.device.rfidMode = user.device.rfidMode == enums.rfidMode.REVOKE ? enums.rfidMode.REVOKE : enums.rfidMode.WRITE;
	    $.mobile.loading('show');
	    user.device.disableUI();
	    
	    // The most elegant solution!
	    // disable key events, will return true when complete
	    
        console.log("disabling key events");
        
        // stop any ongoing scans
        alien.rfid.stop_scan(function(){
            // set a timeout in case write never finishes
            user.device.writeTimeout = setTimeout(function(){
                if (user.device.isWriting){
                    user.device.writeRfidError("RFID Write Timed Out");
                }
            }, 20000);
            user.device.isWriting = true;
            console.log("writing " + data + " to RFID chip!");
            user.device.writeOffset = 2;
            user.device.writeData = data;
           // user.device.recursiveWrite(user.device.writeOffset, data);
            alien.rfid.start_writeTagMemory(format, data, user.device.writeOffset, function(tag){
                user.device.isWriting = false;
	            console.log("Verifying write...");
	            
	            alien.rfid.start_readTagSingle(user.device.printMsg, user.device.writeRfidError);
            }, user.device.writeRfidError);
    
        },  user.device.writeRfidError);
	    
	    
	},
	writeRfidError : function(msg){
	  clearTimeout(user.device.writeTimeout);
	  user.device.isWriting = false;
	  user.device.rfidMode = enums.rfidMode.WRITE;
	  console.log("Write error");
	  console.log(msg);
	  alert(msg);
	  alien.rfid.stop_scan(function(){
	      user.device.init();
	      $.mobile.loading('hide');
	      user.device.enableUI();
	  }, user.device.printMsg);
	    
	},
	revokeTag: function(){
	    console.log("Revoking tag!");
        user.device.rfidMode = enums.rfidMode.REVOKE;
        if (user.device.disableTrigger()){
            user.device.writeRfid('000000000000000000000000');  
        }
	},
    stopRFID : function(key) {
        return alien.rfid.stop_scan(user.device.printMsg, user.device.printMsg);

    },
    startReadRFID : function(key) {
        
        if (user.device.rfidMode == enums.rfidMode.READ){
            if (!user.device.isReading && !user.device.processingTag){
                  alien.rfid.isRunning(function(msg){
                      if (msg == 'false'){
                          user.device.isReading = true; 
                          alien.rfid.start_readTagContinuous(function(msg){console.log(msg);}, user.device.printMsg, false);
                      }
                  }, user.device.printMsg);
            }
        }
        else if (user.device.rfidMode == enums.rfidMode.INVENTORY){
            alien.rfid.isRunning(function(msg){
                  if (msg == 'false'){
                      user.device.isReading = true; 
                      alien.rfid.start_readTagContinuous(function(msg){console.log(msg);}, user.device.printMsg, false);
                  }
              }, user.device.printMsg);
        }
        
    },
    startWriteRFID: function(){
      if (typeof user.device.activeTag == 'undefined' || user.device.activeTag == ''){
            console.log("active tag not found, aborting write...");
            user.device.writeRfidError("active tag not found, aborting write...");
            return;
        }
            
        var data = '';
        for (var i = 0; i < user.device.activeTag.length; i++){
            data += dec2hex(user.device.activeTag.charCodeAt(i));
        }
        
        console.log(data);
        
        user.device.writeRfid(data, 'hex');
          
    },
    changePowerLevel: function(t){
        

        user.device.disableUI();
        
        var $power_selects = $('.power-level');
		var powerLevel = (typeof t == 'undefined') ? '' : $(t).val();
		
		// update preferences with selected power level
		if (typeof user.prefs == 'undefined')
			user.prefs = new Preferences();
		
		if (powerLevel){
			user.prefs.powerLevel = powerLevel;
			
			// update all other power slider selects
    		$.each($power_selects, function(){
    		    $(this).val(powerLevel).selectmenu().selectmenu('refresh');
    		});
		}
		else if (typeof user.prefs.powerLevel == 'undefined'){
			user.prefs.powerLevel = (typeof user.device.powerLevel) != 'undefined' ? user.device.powerLevel : 300;
		}
		
	    
	    user.device.powerLevel = user.prefs.powerLevel;
		
		user.prefs = new Preferences({'powerLevel':user.prefs.powerLevel});
		
		// if the device is initialized, set the power level through the atid rfid api
		if (user.device.hasInitialized){
		    console.log("changing power to: " + user.device.powerLevel);
		    alien.rfid.setPower(user.device.powerLevel / 10, user.device.printMsg, user.device.printMsg); 
		}
		else{
		    console.log("Device couldn't set power, not initialized");
		}
		user.device.enableUI();
		
	},
	populatePowerLevel: function(){
	  // grap select statement by class, dynamically add options 100-300 with increment level 50
	  // once select is populated, make the value = to the loaded preferences option
	  
	  if (typeof user.prefs == 'undefined')
	    user.prefs = new Preferences();
	    
	  if (typeof user.prefs.powerLevel == 'undefined'){
	      user.prefs = new Preferences({'powerLevel':'300'});
	  }
	  var $select = $('.power-level');
	  var options = '';
	  
	  for (var i = 100; i <= 300; i+=50)
	  {
	      options += '<option value="'+i+'">'+i+'</option>';
	  }
	  
	  $.each($select, function(){
	      $(this).html(options);
	      $(this).val( user.prefs.powerLevel ).selectmenu().selectmenu('refresh');
	  });
	  
	},
	switchRfidMode : function(mode){
	  switch(mode){
	      case(enums.rfidMode.READ):
	      case(enums.rfidMode.WRITE):
	          user.device.rfidMode = mode;
	          break;
	      default:
	        console.log("Invalid rfid mode set");
	    
	  }  
	},
    printMsg : function(msg) {console.log(msg)},
    disableUI : function(){
        $("body").prepend("<div class=\"overlay\"></div>");

        $(".overlay").css({
            "position": "absolute", 
            "width": $(document).width(), 
            "height": $(document).height(),
            "z-index": 99999,
            "opacity":0.8,
        }).fadeTo(0, 0.8);
    },
    enableUI : function(){
      $(".overlay").remove();  
    },
    raw:{},
    scanned:{}
};