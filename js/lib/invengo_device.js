var invengo_device = {
    processRawTag: function(data){
        var x = '';
        var prefs = new Preferences();
        
        // check for header hex chars and remove, TODO: find out what header does, scanned tags seem to start with '3000'?
        if (data.length == 28 && prefs.encodingType == enums.encodingType.HEX){
		    x = $.trim(data.substring(4, data.length));
        }
        else
            x = $.trim(data);

		
		// get char code for every 8 bit hex value in raw tag
		var a = x.match(/.{1,2}/g);
		var t = '';
		$.each(a, function(k, v){
		    //console.log("hex transform: " + v);
		    //console.log("corresponding decimal value: " + hex2dec(v));
		    if (prefs.encodingType == enums.encodingType.HEX){
			    t += $.trim(String.fromCharCode(hex2dec(v)));
		    }
		    else {
		        t += $.trim(v);
		    }
		});
		console.log("processed tag: "+t);
		return t;
    }
}