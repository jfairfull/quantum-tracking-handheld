/* global deparam */
/* global __ */
/* global simpleDB */
/* global DB */
/* global $ */
function Server(request, callback) {
    var db = new simpleDB('server');
    var d = db.get();
    this.data = d.data;
    this.forms = d.form;
    this.rfid = d.rfid;
    this.alias = d.alias;
    this.barcode = d.barcode;
    this.requestString = request;
    this.request = deparam(request);
    window.$_REQUEST = this.request;
    this.response = {
        error:{},
        output:{}
    };
    
    this.checkSession = function(){
    	this.response['output']['logged_in'] = true;
        return true;
    };
    
    this.proCreateReport = function(){
		if(__('project') && __('data') && __('name')) {
			this.save();
		} else {
			this.response['error']['error'] = 'Please provide a project, report data and a name for the report.';
		}    	
    };

	this.form = function(type) {
	    if(typeof this.forms[type] != 'undefined') {
	        return this.forms[type];
	    } else {
	        if(typeof this.alias[type] != 'undefined') {
	            return this.forms[this.alias[type]];
	        }
	    }
	    return false;
	};
    
    this.getProjectMeta = function() {
		if(__('project')) {
		    this.response['output'] = this.project(this.request.project);
		    this.response['output']['form'] = this.form(this.request.project);
		} else {
		    this.response['error']['project'] = 'Please provide a project ID.';
		}        
    };
    
    this.proEdit = function(){
    	if(__('project')) {
    		if(__('data')) {
    			this.save();
    		} else {
    			this.response['error']['data'] = 'Please provide some data.';
    		}
    	} else {
    		this.response['error']['project'] = 'Please provide a project ID.';
    	}
    };
    
    this.proFindTag = function(){
        if(__('barcode') || __('tag')) {
            var thing = 'barcode';
            var otherThing = 'tag';
            if(__('barcode')) {
                var db = new DB('barcode');
                var otherDB = new DB('rfid');
                var value = this.request.barcode;
            }
            if(__('tag')) {
                var db = new DB('rfid');
                var otherDB = new DB('barcode');
                var thing = 'tag';
                var otherThing = 'barcode';
                var value = this.request.tag;
            }
            var fetch = db.fetch(thing, value);
            if(fetch) {
                var index = this.data[fetch.type][fetch.content_id];
                var otherFetch = otherDB.fetch('content_id', fetch.content_id);
                var info = $.extend(fetch, otherFetch);
                this.response['output'] = {
                    'tag':info.tag,
                    'id':info.content_id,
                    'project':fetch.type,
                    'data':index
                };
            } else {
                this.response['error']['error'] = 'Not found';
            }
        } else {
            this.response.error['error'] = 'Please provide a barcode or tag value.';
        }
    };
    
    this.proGet = function(){
		if(__('project')) {
		    var project = this.project(this.request.project);
		    var d = this.data[this.request.project];
		    var data = {};
		    $.each(d, function(key, value){
		        data[key] = value;
		        $.each(project.fields, function(k, field){
		            if(typeof data[key][field] == 'undefined') {
		                data[key][field] = '';
		            }
		        });
		    });
		    this.response['output']['fields'] = project.fields;
		    this.response['output']['form'] = this.form(this.request.project);
		    this.response['output']['data'] = data;
		} else {
			this.response['error']['project'] = 'Please provide a project ID.';
		}
    };
    
    this.proItem = function(){
		if(__('project') && __('id')) {
			var item = this.data[this.request.project][this.request.id];
			if(typeof item == 'undefined') {
				item = false;
			} else {
				var db = new DB('rfid');
				var info = db.fetch('content_id', this.request.id);
				item.tag = info.tag;
				db = new DB('barcode');
				info = db.fetch('content_id', this.request.id);
				item.barcode = info.barcode;
			}
			this.response['output'] = item;
		} else {
			this.response['error']['error'] = 'Please provide a project ID and an item ID.';
		}
    };

	this.project = function(type) {
	    var form = this.form(type);
	    var fields = [];
	    if(typeof form == 'object') {
	        $.each(form.form, function(key, field){
	            fields.push(field.name);
	        });
	    }
	    return {
	        name:type,
	        fields:fields,
	        numOfDivs:5,
	        searchForm:form.name,
	        entryForm:form.name,
	        batchUpdateForm:form.name,
	        checkInOutForm:form.name,
	        barcodeField:'',
	        counter:0,
	        counterPrefix:'',
	        switchField:'',
	        minimumQuantity:'',
	        quantityAdmin:''
	    };
	};
    
    this.proSearch = function(){
		if(__('project') && __('data')) {
		    var db = new DB('data', this.request.project);
		    var query = db.query(decodeURI($.param(this.request.data)).replace(/\+/g, ' '));
		    if(query) {
		    	var project = this.project(this.request.project);
		    	this.response['output']['fields'] = project.fields;
		    	this.response['output']['meta'] = {
		    		count:query.length,
		    		max:10000,
		    		start:0
		    	};
		    	this.response['output']['data'] = query;
		    } else {
		    	this.response['error']['error'] = 'No results';
		    }
		} else {
			this.response['error']['error'] = 'Please provide a search query.';
		}
    };
    
    this.save = function(){
    	var db = new simpleDB('serverOperations');
    	var ops = (db.get('edits') ? db.get('edits') : {});
    	delete this.request.token;
    	var hash = CryptoJS.SHA1(JSON.stringify(this.request));
    	ops.push({
    		'hash':hash,
    		'request':this.request
    	});
    	db.put('edits', ops);    	
    };
    
    if(typeof this[this.request.action] == 'function') {
        this[this.request.action]();
        if(typeof callback == 'function') {
            callback(this.response);
        }        
    };    
}