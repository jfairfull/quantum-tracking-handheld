var nativeScan = {
	scanned: {},
	raw: {},
	reads:'',
	isBluetooth:false,
	batteryPercentage:'',
    range:{
        low: -80,
        high: -60
    },
    batteryBar: false,
	find: function(t, type){
		var myType = (type == 'undefined' ? 'rfid' : type);
		var apiCallType = (myType == 'rfid' ? 'tag' : 'barcode');
		if(t.length == 6 || t.length == 12 || t.length == 13) {
			var found = false;
			api.call('action=proFindTag&'+apiCallType+'='+t+'&token='+user.session.user.token, function(response){
				item.display(response, t, 'rfid');
				$.mobile.loading('hide');
			}, function(){
				$.mobile.loading('hide');
			});
			$('body').trigger('findTag');
		} else {
			//alert('The tag is not valid.');
		}
	},
	
	go: function(data, scanType){
	    console.log("in go function");
		if(typeof data != 'undefined') {
		    console.log("data defined");
			if (scanType == 'RFID'){
			    console.log("scanning rfid");
			    if (typeof user.device.raw[data] == 'undefined') {
			        // if we are reading in a new tag into the system, shut down key event listener, stop any scan, process tag/find, then reinitialize
			        user.device.raw[data] = data;
            		$.mobile.loading('show');
            		var t = data;
            		
            	    console.log('Encoded RFID value: ' + t);
            	    t = t.toLowerCase();
            		if(typeof user.device.scanned[t] == 'undefined') {
            		    t = t.toLowerCase();
            			user.device.scanned[t] = t;
            			//notification.beep(1);
            			navigator.notification.beep(1);
            			if($.mobile.activePage.attr("id") == 'inventory') {
            				inventory.refresh(t);
            			} else {
            				user.device.find(t, 'rfid');
            			}
            		}
    			}
			}
            else if (scanType == 'Barcode'){
                console.log("finding barcode");
        		$.mobile.loading('show');
        		var x = data;
        		if(typeof user.device.scanned[x] == 'undefined') {
        		    x = x.toLowerCase();
        			user.device.scanned[x] = x;
        			//user.device.find(x, 'barcode');
        			if($.mobile.activePage.attr("id") == 'inventory') {
            			inventory.refresh(x, 'barcode');
        			} else {
        				user.device.find(x, 'barcode');
        			}
    				
    			}
            }
			
		}
	},

    init: function(t){
		if(cordovaApp) {
			if(typeof user.device == 'undefined') {
				user.changeDevice();
			}
			console.log("connecting native scanner");
			// set up events to enable barcode and rfid scan calls and to enable/disable appropriate buttons
			var htmlString = ''+
			'<div class="nativeFunctions">'+
    	      '<button class="ui-btn nativeBarcode">Scan Barcode</button>'+
    	      '<button class="ui-btn nativeNFC">Scan NFC</button>'+
    	    '</div>';
    	    
    	    var writeNfcbutton = ''+
    	    '<button class="ui-btn nativeWriteNFC">Write NFC</button>'; 
    	    
    	    user.device.removeScanningOptions();
    	    
    	    $('#find .ui-content > div').first().after(htmlString);
    	    $('#inventoryFunctions').after(htmlString);
    	    $('#revoke').after(writeNfcbutton);
    	    
    	    
			$('.nativeBarcode').click(user.device.scanBarcode);
			
			$(document).on('click', '.nativeNFC:not(.active)', user.device.scanNfc);
			$(document).on('click', '.nativeNFC.active', user.device.stopScanNfc);
			
			$('.nativeWriteNFC').click(user.device.writeNfc);
			
		} else {
			$.mobile.loading('hide');
			if(config.apiUrl.indexOf('filelabel.co') != -1) {
				user.device.find('fafc7ced521e');
			} else {
				user.device.find('57477e6090bc5');
			}
		}
    },
    scanBarcode: function(){
         cordova.plugins.barcodeScanner.scan(
              function (result) {
                //   alert("We got a barcode\n" +
                //         "Result: " + result.text + "\n" +
                //         "Format: " + result.format + "\n" +
                //         "Cancelled: " + result.cancelled);
                        
                  // call go function here
                  console.log("got barcode");
                  console.log(result.text);
                  user.device.go(result.text, 'Barcode');
              },
              function (error) {
                  alert("Scanning failed: " + error);
              },
              {
                  "preferFrontCamera" : false, // iOS and Android
                  "showFlipCameraButton" : true, // iOS and Android
                  "showTorchButton" : true, // iOS and Android
                  "prompt" : "Place a barcode inside the scan area", // supported on Android only
              }
           );
    },
    scanNfc: function(){
        nfc.enabled(
        	function(){
        		console.log("nfc is enabled on this device");
        		
        		nfc.addNdefListener(user.device.scanNfcCallback, function(){
        			console.log("added tag discovered listener")
        			$('.nativeNFC').addClass('active');
        			$('.nativeNFC').text('Stop NFC Scan');
        		}, function(){console.log("failed to add discovered tag listener")});
        	}, 
        	function(reason){
        		console.log("nfc is not enabled");
        		console.log(reason);
        	}
        );
    },
    stopScanNfc: function(){
    	nfc.removeNdefListener(user.device.scanNfcCallback,
    	function(){
    		// successfully removed listener, remove button class and change back text
    		$('.nativeNFC').removeClass('active');
        	$('.nativeNFC').text('Scan NFC');
    		
    	}, 
    	function(){
    		console.log("failed to remove nfc tag listener");
    	});
    },
    scanNfcCallback: function(data){
    	console.log("calling scan nfc callback");
    	console.log("tag type: "+data.tag.ndefMessage[0].type);
    	console.log("tag payload: "+data.tag.ndefMessage[0].payload);
    	
    // 	var payload = data.tag.ndefMessage[0].payload.split(',');
    // 	for (a in payload ) {
    //         payload[a] = parseInt(payload[a], 10);
    //     }
    
        var payload = data.tag.ndefMessage[0].payload;
        
        var tag = nfc.bytesToString(payload).substring(3);
    	
    // 	for (var i = 0; i < payload.length; i++){
    // 	    tag += String.fromCharCode(payload[i]);
    // 	}
    	
    	console.log("tag value: "+ tag);
    	user.device.go(tag, 'RFID');
    },
    writeNfc: function(){
    	if (typeof user.device.activeTag == 'undefined' || user.device.activeTag == ''){
            console.log("active tag not found, aborting write...");
            alert("active tag not found, aborting write...");
            return;
        }
        
        console.log("writing tag: ");
        console.log(user.device.activeTag);
        var message = [
		    ndef.textRecord(user.device.activeTag)
		];
		
		nfc.write(message, function(){
			console.log("nfc write success");
			alert("successfully wrote to nfc tag");
			
		}, function(){
			alert("write error");
		});
    },
    revokeTag: function(){
      
		nfc.erase(function(){
			console.log("nfc write success");
			alert("successfully erased nfc tag");
			
		}, function(){
			alert("write error");
		});  
    },
    disconnectDevice: function(){
      user.device.removeScanningOptions();  
    },
    removeScanningOptions: function(){
        console.log("removing buttons");
        $('.nativeFunctions').remove();
        $('.nativeWriteNFC').remove();
    },
    printMsg: function(msg){
    	console.log(msg);
    }
};