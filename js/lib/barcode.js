var barcode = {
	find: function(t){
		var found = false;
		if(api.checkConnection()) {
			api.call('action=proFindTag&barcode='+t, function(response){
				$('#find .ajaxDisplay').append(library.displayObject(response.output.data));
			});
		} else {
			var db = new simpleDB('request');
			var requests = db.get();
			if(requests) {
				$.each(requests, function(key, request){
					if(typeof request.output.tags != 'undefined') {
						$.each(request.output.tags, function(k, v){
							if(t == v) {
								$('#find .ajaxDisplay').append(library.displayObject(response.output.data));
								found = true;
							}
						});
					}
				});				
			} else {
				alert('You are offline and no projects were found.');
			}
		}
		$('body').trigger('findTag');
	}
};