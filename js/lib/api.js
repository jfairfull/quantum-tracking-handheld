var api = {
	url: config.apiUrl,
	call: function(data, callback, error){
		//var requestHash = CryptoJS.SHA1(data);
		var db = new simpleDB('request');
		if(api.checkConnection()) {
			// If we're online, connect to the server.
			$('body').trigger('online');
			var requestString = config.apiUrl+'?jsoncallback=?'+'&'+data+'&compressOutput';
			console.log(config.apiUrl+'?jsoncallback=?'+'&'+data+'&compressOutput');
			if (requestString.length > 7000){
				console.log("Doing post instead");
				$.post(config.apiUrl, data ,function(response){
					if(library.processResponse(response)) {
					//	db.put(requestHash, response);
						if(typeof callback !== 'undefined') {
							callback(response);
						}
					} else {
						if(typeof error !== 'undefined') {
							error(response);
						}					
					}
				});
			}
			else{
			    $.getJSON(config.apiUrl+'?jsoncallback=?'+'&'+data+'&compressOutput', function(response) {
					if(library.processResponse(response)) {
					//	db.put(requestHash, response);
						if(typeof callback !== 'undefined') {
							callback(response);
						}
					} else {
						if(typeof error !== 'undefined') {
							error(response);
						}					
					}
			    });
			}
		} else {
			var server = new Server(data, function(response){
				if(library.processResponse(response)) {
					if(typeof callback !== 'undefined') {
						callback(response);
					}
				} else {
					if(typeof error !== 'undefined') {
						error(response);
					}					
				}
			});
		}
	},

	checkConnection: function() {
		if(window.myOffline) {
			return false;
		}
		if(cordovaApp) {
			var networkState = navigator.connection.type;
			if(networkState == navigator.connection.NONE /*Connection.NONE*/) {
				onConnexionError();
				return false;
			} else {
				return true;
			}			
		} else {
			return navigator.onLine;
		}
	},

	isLoggedIn: function(callback, error){
		theUser = window.theUser;
		if(theUser) {
			if(typeof theUser.token == 'undefined') {
				if(typeof error !== 'undefined') {
					error();
				}
			} else {
				api.call('action=checkSession&token='+theUser.token, function(response){
					if(response.output.logged_in) {
						if(typeof callback != 'undefined') {
							callback();
						}
					} else {
						if(typeof error != 'undefined') {
							error();
						}				
					}
				});
			}
		} else {
			if(typeof error !== 'undefined') {
				error();
			}
		}
	},

	requestType: function(queryString){
		var operations = {
			'proFindTag':'get',
			'proGet':'get',
			'proTag':'put'
		}
		for(var key in operations) {
			if(operations.hasOwnProperty(key)) {
				if(String(queryString).indexOf(key) != -1) {
					return operations[key];
				}
			}
		}
		return 'get';
	}
}