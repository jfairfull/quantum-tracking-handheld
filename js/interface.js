var interFace = {
	clear: function(t) {
		var el = $(t).attr('data-clear');
		$(el).html('');
	},
	
	init: function(){
		var d = new Date();
		var t = d.getTime();
		var y = new Date(t).format('Y');
		$('.date').html(y);
		
		$('<input>').appendTo('.battery-progress-bar').attr({'name':'slider','id':'battery-slider','data-highlight':'true','min':'0','max':'100','value':'50','type':'range'}).slider({
            create: function( event, ui ) {
                $(this).parent().find('input').hide();
                $(this).parent().find('input').css('margin-left','-9999px'); // Fix for some FF versions
                $(this).parent().find('.ui-slider-track').css('margin','0 15px 0 15px');
                $(this).parent().find('.ui-slider-handle').hide();
            }
        }).slider("refresh");
	},

	loginForm: function(){
		var myprefs = new Preferences();
		interFace.loginForm = new Form({
			target:'#logIn .form',
			form:forms.login,
			bootstrap:false,
			// data:{
			// 	'email':'jfairfull@simplicitysolutionsgroup.com',
			// 	'password':'demo1'
			// },
			data: myprefs.loginData ? myprefs.loginData : {
				'email':'',
				'password':''
			},
			event: function(){
				$.mobile.loading('show');
				app.changeUrl();
				api.call($('#logIn form').serialize(), function(response){				
					var db = new simpleDB('user');
					db.put('session', response.output);
					user.session = response.output;
					user.account();
					interFace.projects();
					var prefs = new Preferences({
						apiUrl:api.url,
						locationField: response.output.locationField
					});
					config.apiUrl = api.url;
					user.prefs = prefs;
					$('#apiUrl').val(prefs.apiUrl).textinput().textinput('refresh');
					console.log(prefs);
					api.url = prefs.apiUrl;
					$.mobile.loading('hide');
					$.mobile.changePage("#home");
				}, function(){
					$.mobile.loading('hide');
				});
			}
		}, function(f){
			$('#logIn form').attr('data-ajax', 'false');
			$('#logIn form').find('button').parents('div:first').removeAttr('class');
		});
	},
	
	projects: function(){
		var db = new simpleDB('user');
		var session = db.get('session');
		user.session = session;
		app.changeUrl();
		user.prefs = new Preferences();
		$('#device').val(user.prefs.device).selectmenu().selectmenu('refresh');
		interFace.initEncodingSelector();
		
		//user.changeDevice();
		
		user.account();
		var projects = '<option value="">Please Select a Project</option>';
		if(session) {
			try {
				$.each(session.user.projects, function(key, value){
					projects += '<option value="'+key+'">'+library.key(value.name)+'</option>';
				});			
			} catch(e) {}
		}
		$('#projects').html(projects);
		
		
		// ####### PURGE AFTER TESTING ###############################################################################################################################
		// var myobj = {};
		// for (var i = 0; i < 10; i++){
		// 	myobj[i] = 'hello';
		// }
		// myobj.hello = "world";
		// myobj.world = "hello";
		
		// var db = new simpleDB('customTable');
		// var i = 0;
		// setInterval(function(){
		// 	db.put(i+'f', myobj);
		// 	i++;
		// }, 30);
		
		// ################################################################################################################################################################
	},
	initEncodingSelector: function(){
		 var prefs = new Preferences();
		// if (!prefs.encodingType){
		// 	prefs.encodingType = enums.encodingType.HEX;
		// }
		if (!prefs.encodingType){
			prefs.encodingType = enums.encodingType.HEX;
			prefs = new Preferences(prefs);
		}
		
		$('.encodingType').val(prefs.encodingType).selectmenu().selectmenu('refresh');
		
	},
	
	selector: function(t){
		$(t).parents('.selector:first').find('button').removeClass('active');
		$(t).addClass('active');
		var type = $(t).attr('data-type');
		$('.scan').attr('id', type);
	},
	initPowerSlider: function(){
		prefs = new Preferences();
		$('.power-slider').val(prefs.powerSlider);
	},
	addScanButtons: function(text, classname, startReadCallback, stopReadCallback, writeCallback){
		console.log("adding scan buttons");
		var htmlString = ''+
		'<div class="nativeFunctions">'+
	      '<button class="ui-btn nativeBarcode">Scan Barcode</button>'+
	      '<button class="ui-btn '+classname+'Scan">Scan '+text+'</button>'+
	    '</div>';
	    
	    var writeButton = ''+
	    '<button class="ui-btn write'+classname+'">Write NFC</button>'; 
		
		
		$('#find .ui-content > div').first().after(htmlString);
	    $('#inventoryFunctions').after(htmlString);
	    $('#revoke').after(writeButton);
	    
	    if (typeof user.device != 'undefined' && user.device.scanBarcode != 'undefined'){
			$('.nativeBarcode').click(user.device.scanBarcode);
	    }
		
		$(document).on('click', '.'+classname+'Scan:not(.active)', function(){
			var success = startReadCallback();
			console.log(success);
		
			$( '.'+classname+'Scan').addClass('active');
    		$( '.'+classname+'Scan').text('Stop '+text+' Scan');
			
			
		});
		$(document).on('click', '.'+classname+'Scan.active', function(){
			var success = stopReadCallback();
			
			//if (success){
				$( '.'+classname+'Scan').removeClass('active');
    			$( '.'+classname+'Scan').text('Start '+text+' Scan');	
			//}
			
		});
		
		$('.write'+classname).click(writeCallback);
	},
	removeScanButtons: function(classname){
		 console.log("removing buttons");
		$('.nativeFunctions').remove();
        $('.write'+classname).remove();
	},
	displayFindImages: function(){
		var $ajaxView = $('#find .ajaxDisplay');
		console.log($ajaxView);
		$ajaxView.find('pre').each(function(key, val){
			if ($(this).find('img').length == 0){
				console.log($(this).data('image'));
				console.log($(this).attr('data-image'));
				if ($(this).data('image')){
					var image = $(this).data('image');
					var fileUrl = '';
					// if image is just id, append the base url to the end
					if (image.split('/').length = 1){
						var prefs = new Preferences();
						var apiUrl = prefs.apiUrl;
						
						var urlsegs = apiUrl.split('/');
						var baseUrl = urlsegs[2];
						fileUrl = 'https://'+baseUrl+'/file::/'+image;
					}
					else {
						fileUrl = image;	
					}
					
					console.log(fileUrl);
					
					var imageTag = '<img class="findItemImage" style="max-width:100px;height:auto;" src="'+fileUrl+'" />';
					$(this).html(imageTag+'</br>'+$(this).html());
				}
			}
		});
	},
	getLists: function(){
        api.call('action=proGetLists&token='+user.session.user.token, function(response){
            $('#viewLists').html('<option value="">Choose List</option>');
            
            if(response.output) {
                $.each(response.output, function(key, value){
                    //console.log(key);
                    $('#viewLists').append('<option value="'+key+'">'+value.name+'</option>');
                   
                });
                $('#viewLists').selectmenu().selectmenu('refresh');
              
            }
        });
    },
    getItemTree: function(t){
    	console.log("clicked item tree button");
    	$('#tree .ui-content').html('');
    	$.mobile.changePage( "#tree", { role: "dialog" } );
    	var id = $(t).data('id');
    	console.log(id);
    	api.call('action=proGetTree&id='+id+'&token='+user.session.user.token, function(response){
    		
    		if (response.output){
    			var margin = 0;
    			$.each(response.output, function(key, value){
    				if(typeof value.name == 'undefined') {
						for(var first in value) break;
						var displayField = value[first];
						$.each(value, function(l, w){
							if(l != 'type' && String(l).toLowerCase().indexOf('type') != -1) {
								displayField = w;
								return;
							}
						});					
					} else {
						var displayField = value.name;
					}
					$('#tree .ui-content').append('<p style="margin-left:'+margin+'px"><b style="text-transform:capitalize;">'+value.type+':</b> '+displayField+'</p>');
					margin = margin + 20;
    				
    			});
    		}	
    	});
    },
	
};