var item = {
	display: function(response, val, type){
		var t = 'data-barcode';
		if(type == 'rfid' ) {
			var t = 'data-tag';
		}
		if(typeof response.output.data != 'undefined') {
			
			var $treebtn = $('<a data-id="'+response.output.id+'" class="tree-btn">View Tree</a>');
			console.log('my image: '+response.output.data.image);
			$('#tagDisplay').append(library.displayObject(response.output.data));					
			$('#tagDisplay').find('pre:last-child')
			.attr(t, val)
			.attr('title', 'Edit')
			.attr('data-uri', $.param(response.output.data))
			.attr('data-project', response.output.project)
			.attr('data-id', response.output.id)
			.attr('data-image', response.output.data.image).after($treebtn);
			$.mobile.loading('hide');
		} else {
			// alert('Not found.');
			console.log("tag not found");
		}
	},
	
	edit: function(t){
		var data = deparam($(t).attr('data-uri'));
		var id = $(t).attr('data-id');
		var project = $(t).attr('data-project');
		$("body, pre").css("cursor", "progress");
		api.call('action=getProjectMeta&project='+project+'&token='+user.session.user.token, function(response){
			$("body, pre").css("cursor", "default");
			if(response.output.form) {
				var f = new Form({
					target:'#formModal .modal-body',
					form:response.output.form.form,
					bootstrap:false,
					data:data,
					event: function(t, e){
						var o = {};
						o.data = {};
						o.data[id] = $('#formModal form').serializeObject();
						var d = $.param(o);
						api.call('action=proEdit&token='+user.session.user.token+'&project='+project+'&'+d, function(response){
							console.log("successfully edited data");
							$('pre[data-id="'+id+'"]').fadeOut('fast', function(){
								$(this).remove();
								$('.tree-btn[data-id="'+id+'"]').remove();
								var tags = $('#tagDisplay pre').length;
								if(tags) {
									$('#tagDisplay pre:first-child').trigger('click');
								} else {
									$.mobile.changePage('#find');
								}
							});
						}, function(err){
							console.log(err);
						});
					}
				}, function(){
					$('#formModal form').attr('data-ajax', 'false');
					$('#formModal form').find('button').parents('div:first').removeAttr('class');
					forms.refresh('#formModal form', data);
					$.mobile.changePage('#formModal', 'pop', true, true);
				});
			}
		});
	},
	
	show: function(response, val){
		$('#commissionDisplay').html(library.displayObject(response.output.data));
		$('#commissionDisplay').find('pre')
		.attr('data-tag', response.output.tag)
	}
};