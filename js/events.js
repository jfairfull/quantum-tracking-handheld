Ready(mobileApp.initialize);
Click('.selector button', interFace.selector);
Change('#projects', project.load);
Change('#viewLists', project.loadList);
Click('#find pre', item.edit);
Click('#inventory pre', item.edit);
Click('.clear', interFace.clear);
Click('.createList', project.createReport);
Click('.createReport', project.createReport);
Click('.inventoryReport', inventory.createReport);
Click('.inventoryList', inventory.createReport);
Click('.inventoryNotInListReport', inventory.createReport);
Click('#logOut', user.logOut);
Click('#forgotPassword', user.forgotPassword);
Submit('#account-form', user.editAccount);
Change('#device', user.changeDevice);
Submit('#globalUpdate form', project.globalUpdate);
Submit('#inventoryAppendList form', inventory.appendList);
Click('#inventoryUpdate', inventory.globalUpdate);
Click('#clearFinder', app.clearFinder);
Click('#search', project.searchForm);
Keyup('#apiUrl', app.changeUrl);
Click('#enterUrl', app.enterUrl);
Click('#clearData', app.clear);
Click('#proData', app.proData);
Change('#inventoryLocations select', inventory.changeLocation);
Change('#inventoryLists', inventory.selectList);
Click('#clearInventory', inventory.clear);
Click('#runInventory', inventory.run);
Click('.writeTag', project.writeTag);
Click('.attachImage', project.attachImage);
Click('.tree-btn', interFace.getItemTree);
Change('.encodingType', app.switchEncodingType);
Click('.myTestInput', function(){
   if (user.device.className == 'alrh450'){
       alien.barcode.isRunning(function(msg){
          if (msg == "false"){
              alien.barcode.start_scan(user.device.printMsg, user.device.printMsg);
          } 
          else{
              alien.barcode.stop_scan(user.device.printMsg, user.device.printMsg);
          }
       });
   } 
});
Click('#revoke', function(){
    // console.log("Revoking tag!");
    // user.device.rfidMode = 'revoke';
    // user.device.writeRfid('000000000000000000000000');
    
    if (typeof user.device.revokeTag != 'undefined'){
        user.device.revokeTag();
    }
});
Click('#displayFindImages', interFace.displayFindImages);

$(document).on('appReady', 'body', function(){
    document.addEventListener('DOMContentLoaded', function() {
        FastClick.attach(document.body);
    }, false);
    app.init();
    $.fn.dataTable.ext.errMode = 'none';
    
    // var i = setInterval(function(){
    //     if(user.device != 'undefined' && user.loggedIn()) {
    //         clearInterval(i);
    //         //user.changeDevice(); // already called in app.init through interface.projects?
    //         //user.device.init();
            
    //     }
    // }, 1000);
});
$(document).on('deviceready', function(){
   if (typeof device.model != 'undefined' && device.model.toUpperCase() == 'AT911N'){
      //window.cache.clear( function(){console.log("cleared cache")}, function(){console.log("failed to clear cache")} );
      //app.removeAllCache();
      window.cache_cleaner.delete(function(){console.log("cache clean success");}, function(){console.log("cache clean fail");});
   }
   var i = setInterval(function(){
        if(user.device != 'undefined' && user.loggedIn()) {
            clearInterval(i);
            user.changeDevice(); // already called in app.init through interface.projects?
            //user.device.init();
            
        }
    }, 1000); 
});
$(document).on('locationConfirmed', '#inventory', function(){
    inventory.getLocation(inventory.sort);
});

$(document).on('battery-indicator-update', function(e, batteryPercentage){
    if (batteryPercentage){
        // $('.battery-progress-bar').css('width', batteryPercentage+'%');
        // $('.battery-progress-bar').text(batteryPercentage+'%');
        
        if (typeof user.device.batteryBar != 'undefined' && user.device.batteryBar){
            console.log("setting the value for the progress bar");
            user.device.batteryBar.setValue(parseInt(batteryPercentage));
        }
        
    } else {
        // $('.battery-progress-bar').css('width', batteryPercentage);
        // $('.battery-progress-bar').text(batteryPercentage);
        if (typeof user.device.batteryBar != 'undefined' && user.device.batteryBar){
            user.device.batteryBar.destroy();
        }
    }
});

$(document).on('pagechange', function(e, data) {
    // set default of read
    if (typeof user.device != 'undefined'){
        user.device.rfidMode = 'read';
    }
    var page = $(data.toPage).attr('id');
    switch(page) {
        case 'inventory' :
            if (typeof user.device != 'undefined'){
                user.device.rfidMode = 'inventory';
            }
            inventory.ui();
            break;
        case 'tag':
            if (typeof user.device != 'undefined'){
                user.device.rfidMode = 'write';
                
                if (typeof user.device.scanType != 'undefined'){
                    if (user.device.scanType == 'Barcode'){
                        $('#atid-scanType').val('RFID').selectmenu('refresh');
                        user.device.switchScanType('#atid-scanType');
                    }
                }
            }
            break;
        case 'rangeFinder':
            if (typeof user.device != 'undefined'){
                user.device.rfidMode = 'find';
            }
            break;
        case 'settings':
            if (typeof user.device != 'undefined' && typeof user.device.checkConnectionStatus != 'undefined'){
                user.device.checkConnectionStatus();
            }
            break;
        case 'home':
            if (typeof device.model != 'undefined' && device.model.toUpperCase() == 'AT911N'){
                console.log("clearing cache");
                //window.cache.clear( function(){console.log("cleared cache")}, function(){console.log("failed to clear cache")} );
                //app.removeAllCache();
                window.cache_cleaner.delete(function(){console.log("cache clean success");}, function(){console.log("cache clean fail");});
            }    
            break;
        case 'view':
            // interFace.getLists();
            break;
    }
});