var app = {
	changeUrl: function(t){
		if(typeof t == 'undefined') {
			var prefs = new Preferences();
			$('#apiUrl').val(prefs.apiUrl).textinput().textinput('refresh');
			//config.apiUrl = prefs.apiUrl;
			user.prefs = prefs;
			api.url = config.apiUrl;
			var db = new simpleDB('config');
			db.put('config', config);
		} else {
			ui.delay(function(){
				var prefs = new Preferences({
					apiUrl:$(t).val()
				});
				config.apiUrl = prefs.apiUrl;
				var db = new simpleDB('config');
				db.put('config', config);
				user.prefs = prefs;
			}, 350);
		}
	},
	
	clear: function(){
		localStorage.clear();
		user.logOut();
	},
	
	enterUrl: function(){
		var url = prompt('Your URL');
		if(url) {
			var url = String(url).replace(/ /g,'')
			config.apiUrl = url;
			api.url = config.apiUrl;
			var db = new simpleDB('config');
			db.put('config', config);
			$('#apiUrl').val(url).textinput().textinput('refresh');
		}
	},
	
	checkSession: function(){
		console.log("Checking session..");
		var db = new simpleDB('user');
		var session = db.get('session');
		if(session) {
			console.log("session object found, checking remote server..");
			api.call('action=checkSession&token='+session.user.token, function(response){
				if(response.output.logged_in === false) {
					console.log("expired session");
					alert('Your session has expired, please log in.');
					db.del('session');
					app.init();
				}
			}, function(){
				// session has expired
				console.log("expired session");
				alert('Your session has expired, please log in.');
				db.del('session');
				app.init();
			});
		}
	},
	
	clearFinder: function(){
		user.device.scanned = {};
		if (typeof user.device.raw != 'undefined') user.device.raw = {};
		$('#tagDisplay').html('');
	},
	
	init: function(){
		app.changeUrl();
		app.checkSession();
		interFace.init();
		interFace.loginForm();
		/*if ((typeof app.checkSessionInterval != 'undefined') && app.checkSessionInterval)
			clearInterval(app.checkSessionInterval);*/
		app.checkSessionInterval = setInterval(function(){
			app.checkSession();
		}, 300000);		
		api.url = config.apiUrl;
		if(user.loggedIn()) {
			interFace.projects();
		} else {
			$.mobile.changePage("#logIn");
		}
	},
	
	proData: function(){
		api.call('action=proData&token='+user.session.user.token, function(response){
			$.each(response.output, function(key, value){
				var db = new simpleDB('server');
				db.put(key, value);
			});
		});
	},
	removeAllCache: function(){
	    window.resolveLocalFileSystemURL(cordova.file.cacheDirectory, app.gotDirRemove, function(error){});
	},

	gotDirRemove: function(entry){
		console.log("got the directory to remove cache");
		console.log(cordova.file.externalCacheDirectory);
		console.log(entry.name);
		console.log(entry);
	    var directoryReader = entry.createReader();
	    directoryReader.readEntries(function(entries) {
	        console.log("reading entries callback");
	        console.log(entries.length);
	        var i;
	        for (i = 0; i < entries.length; i++) {
	        	console.log(entries[i].name);
	            entries[i].remove(function(file){
	            },function(error){
	                });
	        }
	    },function(error){console.log("Failed to list directory contents: " + error.code)});
	},
	switchEncodingType: function(t){
		var prefs = new Preferences();
		prefs.encodingType = ($(t).val() == 'hex' ? enums.encodingType.HEX : enums.encodingType.ASCII);
		prefs = new Preferences(prefs);
	}
};